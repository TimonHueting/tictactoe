import { Vue } from "vue-class-component";

export default class GameComponent extends Vue {
  protected board: HTMLDivElement | null = null;

  protected isLoading = false;

  protected hasStarted = false;

  protected gameEnded = false;

  protected losses = 0;

  protected wins = 0;

  protected ties = 0;

  protected isPlayer = true;

  protected fields!: Element[];

  protected result: Result | string = "";

  protected async initialize(): Promise<void> {
    this.isLoading = true;

    this.result = "";
    this.gameEnded = false;
    this.hasStarted = true;
    const boards = document.getElementsByClassName("board");
    if (boards) {
      const board = boards[0];
      if (board) {
        board.remove();
      }
    }

    // Fake loading
    setTimeout(() => {
      this.buildGame();
      this.isLoading = false;
    }, 750);
  }

  protected buildGame(): this | void {
    this.board = document.createElement("div");
    this.board.classList.add("board");

    const totalAmountFields = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    for (let i = 0; i < totalAmountFields.length; i++) {
      const field: HTMLDivElement = document.createElement("div");
      field.classList.add("field");
      field.classList.add(`field-${totalAmountFields[i]}`);
      field.addEventListener("click", (event) => {
        this.onFieldSelected(event);
      });

      this.board.appendChild(field);
    }

    const target = document.getElementById("game");
    if (!target) {
      alert("Something went wrong. Please reload the page.");
      return;
    }

    target.appendChild(this.board);

    return this;
  }

  protected onFieldSelected(field: MouseEvent): void {
    this.fields = [...document.getElementsByClassName("field")];
    const clickedField = field.target as HTMLDivElement;
    if (!clickedField) {
      return;
    }

    if (this.isPlayer) {
      if (clickedField.innerHTML) {
        alert("Spot is not available");
        return;
      }

      clickedField.innerHTML = Turn.player;

      const result = this.checkWinner(this.fields);
      if (result !== null) {
        if (result === 1) {
          this.result = Result.win;
          this.wins++;
          this.gameEnded = true;
          this.hasStarted = false;
          return;
        }
        if (result === -1) {
          this.result = Result.loss;
          this.losses++;
          this.gameEnded = true;
          this.hasStarted = false;
          return;
        }
        if (result === 0) {
          this.result = Result.tie;
          this.ties++;
          this.gameEnded = true;
          this.hasStarted = false;
          return;
        }
      }

      this.isPlayer = false;
      this.onComputerTurn();
    }
  }

  protected onComputerTurn(): void {
    let bestScore = Infinity;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let bestMove: any;
    this.fields.forEach((field) => {
      if (!field.innerHTML) {
        field.innerHTML = Turn.computer;
        const score = this.minimax(0, true);
        field.innerHTML = "";

        if (score < bestScore) {
          bestScore = score;
          bestMove = field;
        }
      }
    });

    if (bestMove) {
      const target = bestMove as HTMLElement;
      target.innerHTML = Turn.computer;
    }

    const result = this.checkWinner(this.fields);
    if (result !== null) {
      if (result === 1) {
        this.result = Result.win;
        this.wins++;
        this.gameEnded = true;
        this.hasStarted = false;
        this.isPlayer = true;
        return;
      }
      if (result === -1) {
        this.result = Result.loss;
        this.losses++;
        this.gameEnded = true;
        this.hasStarted = false;
        this.isPlayer = true;
        return;
      }
      if (result === 0) {
        this.result = Result.tie;
        this.ties++;
        this.gameEnded = true;
        this.hasStarted = false;
        this.isPlayer = true;
        return;
      }
    }

    this.isPlayer = true;
  }

  protected minimax(depth: number, isMaximizing: boolean): number {
    const result = this.checkWinner(this.fields);
    if (result !== null) {
      return result;
    }

    if (isMaximizing) {
      let bestScore = -Infinity;
      this.fields.forEach((field) => {
        if (!field.innerHTML) {
          field.innerHTML = Turn.player;
          const score = this.minimax(depth + 1, false);
          field.innerHTML = "";
          bestScore = Math.max(score, bestScore);
        }
      });

      return bestScore;
    } else {
      let bestScore = Infinity;
      this.fields.forEach((field) => {
        if (!field.innerHTML) {
          field.innerHTML = Turn.computer;
          const score = this.minimax(depth + 1, true);
          field.innerHTML = "";
          bestScore = Math.min(score, bestScore);
        }
      });

      return bestScore;
    }
  }

  protected checkWinner(fields: Element[]): number | null {
    let outcome: number | null = null;

    if (
      fields[0].innerHTML === Turn.player &&
      fields[1].innerHTML === Turn.player &&
      fields[2].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[0].innerHTML === Turn.computer &&
      fields[1].innerHTML === Turn.computer &&
      fields[2].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[0].innerHTML === Turn.player &&
      fields[3].innerHTML === Turn.player &&
      fields[6].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[0].innerHTML === Turn.computer &&
      fields[3].innerHTML === Turn.computer &&
      fields[6].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[0].innerHTML === Turn.player &&
      fields[4].innerHTML === Turn.player &&
      fields[8].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[0].innerHTML === Turn.computer &&
      fields[4].innerHTML === Turn.computer &&
      fields[8].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[3].innerHTML === Turn.player &&
      fields[4].innerHTML === Turn.player &&
      fields[5].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[3].innerHTML === Turn.computer &&
      fields[4].innerHTML === Turn.computer &&
      fields[5].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[6].innerHTML === Turn.player &&
      fields[7].innerHTML === Turn.player &&
      fields[8].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[6].innerHTML === Turn.computer &&
      fields[7].innerHTML === Turn.computer &&
      fields[8].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[2].innerHTML === Turn.player &&
      fields[5].innerHTML === Turn.player &&
      fields[8].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[2].innerHTML === Turn.computer &&
      fields[5].innerHTML === Turn.computer &&
      fields[8].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[2].innerHTML === Turn.player &&
      fields[4].innerHTML === Turn.player &&
      fields[6].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[2].innerHTML === Turn.computer &&
      fields[4].innerHTML === Turn.computer &&
      fields[6].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else if (
      fields[1].innerHTML === Turn.player &&
      fields[4].innerHTML === Turn.player &&
      fields[7].innerHTML === Turn.player
    ) {
      outcome = 1;
    } else if (
      fields[1].innerHTML === Turn.computer &&
      fields[4].innerHTML === Turn.computer &&
      fields[7].innerHTML === Turn.computer
    ) {
      outcome = -1;
    } else {
      const hasMoves = this.hasMovesLeft(fields);
      if (!hasMoves) {
        outcome = 0;
      }
    }

    return outcome;
  }

  protected hasMovesLeft(fields: Element[]): boolean {
    let hasMoves = true;

    const filteredFields = fields.filter((field) => !field.innerHTML);
    if (!filteredFields.length) {
      hasMoves = false;
    }

    return hasMoves;
  }
}

enum Turn {
  player = "X",
  computer = "O",
}

enum Result {
  win = "You win!",
  loss = "You lose..",
  tie = "It's a tie. Do you want to try again?",
}
